# Vass Test Plugin

Versión: 1.0
Probado hasta la versión: 5.7.2
Licencia: GPLv2 o superior
URI Licencia: http://www.gnu.org/licenses/gpl-2.0.html

Plugin Demo de Wordpress para la entrevista de Vass.

## Descripción

Este plugin crea nuevos Conditional Tags para el área de administración de Wordpress y permite visualizar en el Toolbar los que estén activos.  

![image-20210702123551925](./docs/image-20210702123551925.png)

### Conditional Tags disponibles

Para habilitar los Conditional Tags requeridos se definieron un grupo de métodos booleanos que se encuentran en la interfaz`src/ConditionalTagsInterface`. La implementación por defecto de esta interfaz con la lógica para cada conditional tag está en la clase `src/ConditionalTags`. 

El uso de la interfaz permite desacoplar la implementación de los conditional tags de la clase principal del plugin `VassTestPlugin` de manera que pueda ser sustituida por otra implementación si es requerido (incluso en tiempo de ejecución mediante un filtro habilitado, ver siguiente sección).

Los métodos son los siguientes:

* **is_list**: Conocer si un usuario se encuentra en la página de listado de post. (De cualquier Custom Post Type o de alguno específico, esto útlimo pasando el post_type como parámetro).
* **is_edit**: Conocer si el usuario se encuentra en la página de edición de algún post. (De cualquier Custom Post Type o de alguno específico,  esto útlimo pasando el post_type como parámetro).
* **is_admin_options_general**: Conocer si el usuario se encuentra en alguna página de Ajustes Generales (o en alguna específica del menú de Ajustes Generales de las que vienen por defecto, esto último pasando el slug de la página como parámetro sin el prefijo `options-` ni el `.php`).
* **is_themes**: Conocer si el usuario se encuentra en la página de temas.
* **is_dashboard**: Conocer si el usuario se encuentra en el dashboard principal.
* **is_plugins**: Conocer si el usuario se encuentra en la página de plugins.
* **is_nav_menus**: Conocer si el usuario se encuentra en la página de menús.
* **is_user_list**: Conocer si el usuario se encuentra en la página de listado de usuarios.
* **is_user_edit**: Conocer si el usuario se encuentra en la página de edición de un usuario.
* **is_tools**: Conocer si el usuario se encuentra en alguna página del menú Herramientas.

Para hacer uso de la implementación de los conditional tags, se puede hacer accediendo a la variable global definida en el plugin que contiene la instancia de la clase del plugin y luego a los métodos antes listados según se requiera.

Ejemplo:

```php
global $vass_test_plugin;
$vass_test_plugin->get_conditional_tag_imp()->is_tools(); 
```

Una manera alternativa sería mediante una función provista por el plugin, que simplifica el código anterior:

```php
conditional_tags_imp()->is_tools();
```

### Filtros disponibles

**vass-test-show-tags-toolbar**

Permite mostrar u ocultar el texto en la barra superior del área de administración (retornando `true` para mostrar o `false` para ocultar) que muestra los conditional tags que están aplicando. Ejemplo:

```php
add_filter('vass-test-show-tags-toolbar', 'disable_toolbar_conditional_tags');
function disable_toolbar_conditional_tags($show_in_toolbar)
{
    return false;
}
```



**vass-test-conditional-tags**

Permite modificar el listado de conditional tags o las funciones/métodos a ser llamados para cada conditional tags y que son utilizados para verificar el uso de cada uno y mostrar en la barra superior del área de administración el texto de los activos.

Recibe un párametro con el array asociativo de los conditional tags a verificar, donde la key es el texto a mostrar en la barra cuando está activo y el value es un array con el método/función a utilizar y los parámetros que se le pasarán al mismo en un array (por defecto un array vacío = ninguno)

Ejemplo de implementación por defecto:

```php
$conditional_tags = array(

      'is_list' => array($this->conditional_tags_imp, 'is_list', array()),
      'is_page_list' => array($this->conditional_tags_imp, 'is_list', array('page')),

      'is_edit' => array($this->conditional_tags_imp, 'is_edit', array()),
      'is_post_edit' => array($this->conditional_tags_imp, 'is_edit', array('post')),
      'is_page_edit' => array($this->conditional_tags_imp, 'is_edit', array('page')),

      'is_options_general' => array($this->conditional_tags_imp, 'is_admin_options_general', array()),
      'is_options_writing' => array($this->conditional_tags_imp, 'is_admin_options_general', array('writing')),
      'is_options_reading' => array($this->conditional_tags_imp, 'is_admin_options_general', array('reading')),
      'is_options_discussion' => array($this->conditional_tags_imp, 'is_admin_options_general', array('discussion')),
      'is_options_media' => array($this->conditional_tags_imp, 'is_admin_options_general', array('media')),
      'is_options_permalink' => array($this->conditional_tags_imp, 'is_admin_options_general', array('permalink')),
      'is_options_privacy' => array($this->conditional_tags_imp, 'is_admin_options_general', array('privacy')),

      'is_themes' => array($this->conditional_tags_imp, 'is_themes', array()),
      'is_dashboard' => array($this->conditional_tags_imp, 'is_dashboard', array()),
      'is_plugins' => array($this->conditional_tags_imp, 'is_plugins', array()),
      'is_nav_menus' => array($this->conditional_tags_imp, 'is_nav_menus', array()),
      'is_user_list' => array($this->conditional_tags_imp, 'is_user_list', array()),
      'is_user_edit' => array($this->conditional_tags_imp, 'is_user_edit', array()),

      'is_tools' => array($this->conditional_tags_imp, 'is_tools', array()),
    );
```

Ejemplo de uso:

```php
add_filter('vass-test-conditional-tags', 'conditional_tags_to_check');
function conditional_tags_to_check($conditional_tags_array)
{
    // Eliminar verificación de is_themes
    unset($conditional_tags_array['is_themes']);
    
    // Agregar un nuevo conditional tag con una función personalizada
    $conditional_tags_array['my_conditional_tag'] = array('my_conditional_tag_func', array());
    
    // Modificar la llamada del conditional tag is_tools por una función personalizada
    $conditional_tags_array['is_tools'] = array('custom_is_tool', array());
    
    return $conditional_tags_array;
}
```



**vass-test-conditional-tags-instance**

Permite modificar la implementación de `ConditionalTagsInterface` que utilizará el plugin. Este filtro tiene la finalidad de inyectar en tiempo de ejecución la dependencia desde otro plugin o el function del tema activo. 

El filtro recibe un parámetro que por defecto es la implementación provista por el plugin de la interfaz antes mencionada. La idea es que se pueda proveer otra implementación distinta en caso necesario o extender la ya existente sin modificar el código del plugin.

Ejemplo:

```php
// Ejemplo de implementación
class CustomConditionalTags implements VassTest\ConditionalTagsInterface
{
    //....
}

// Ejemplo de herencia de la implementación por defecto
class ConditionalTagsChild extends VassTest\ConditionalTags
{
    //....
}
```

```php
// Ejemplo usando implementación de la interfaz
add_filter('vass-test-conditional-tags-instance', 'my_custom_implementation');
function my_custom_implementation($imp)
{
    return new CustomConditionalTags();
}
```

```php
// Ejemplo usando herencia de la implementación por defecto
add_filter('vass-test-conditional-tags-instance', 'my_custom_implementation');
function my_custom_implementation($imp)
{
   return new ConditionalTagsChild();
}
```

En caso de que la nueva implementación cuente con nuevos métodos y se desee que el área habilitada en la barra superior muestre información sobre su estado, se deberá registrar los nuevos métodos mediante el filtro antes explicado.