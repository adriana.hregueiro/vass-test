<?php

namespace VassTest;

use VassTest\ConditionalTagsInterface;

/**
 * VassTest main plugin class
 * 
 * @since 1.0.0
 * @author Adriana <adriana.hregueiro@gmail.com>
 */
class VassTestPlugin
{
  /**
   * Stores a comma separate string with the Conditional Tags 
   * in use to be added to admin top bar
   *
   * @since 1.0.0
   * @var string
   */
  private $current_tag;

  /**
   * Instance of the class having the custom conditional tags methods
   *
   * @since 1.0.0
   * @var ConditionalTagsInterface
   */
  private $conditional_tags_imp;

  /**
   * Undocumented variable
   *
   * @var [type]
   */
  private $conditional_tags;

  /**
   * Constructor
   */
  public function __construct()
  {
    if (apply_filters('vass-test-show-tags-toolbar', true)) {

      add_action('wp_enqueue_scripts', array($this, 'setup_toolbar'));
      add_action('admin_enqueue_scripts', array($this, 'setup_toolbar'));
    }
  }

  /**
   * Init the object attributes
   *
   * @param ConditionalTagsInterface $conditional_tags_imp
   */
  public function init($conditional_tags_imp)
  {
    $this->conditional_tags_imp = $conditional_tags_imp;
    /**
     * Default conditional tags to check
     */
    $conditional_tags = array(

      'is_list' => array($this->conditional_tags_imp, 'is_list', array()),
      'is_page_list' => array($this->conditional_tags_imp, 'is_list', array('page')),

      'is_edit' => array($this->conditional_tags_imp, 'is_edit', array()),
      'is_post_edit' => array($this->conditional_tags_imp, 'is_edit', array('post')),
      'is_page_edit' => array($this->conditional_tags_imp, 'is_edit', array('page')),

      'is_options_general' => array($this->conditional_tags_imp, 'is_admin_options_general', array()),
      'is_options_writing' => array($this->conditional_tags_imp, 'is_admin_options_general', array('writing')),
      'is_options_reading' => array($this->conditional_tags_imp, 'is_admin_options_general', array('reading')),
      'is_options_discussion' => array($this->conditional_tags_imp, 'is_admin_options_general', array('discussion')),
      'is_options_media' => array($this->conditional_tags_imp, 'is_admin_options_general', array('media')),
      'is_options_permalink' => array($this->conditional_tags_imp, 'is_admin_options_general', array('permalink')),
      'is_options_privacy' => array($this->conditional_tags_imp, 'is_admin_options_general', array('privacy')),

      'is_themes' => array($this->conditional_tags_imp, 'is_themes', array()),
      'is_dashboard' => array($this->conditional_tags_imp, 'is_dashboard', array()),
      'is_plugins' => array($this->conditional_tags_imp, 'is_plugins', array()),
      'is_nav_menus' => array($this->conditional_tags_imp, 'is_nav_menus', array()),
      'is_user_list' => array($this->conditional_tags_imp, 'is_user_list', array()),
      'is_user_edit' => array($this->conditional_tags_imp, 'is_user_edit', array()),

      'is_tools' => array($this->conditional_tags_imp, 'is_tools', array()),
    );

    $this->conditional_tags = apply_filters('vass-test-conditional-tags', $conditional_tags);
  }

  /**
   * Add toolbar message according to the current conditional tag detected
   * to the wp-admin top bar
   *
   * @since 1.0.0
   * @param WP_Admin_Bar $wp_admin_bar
   * @return void
   */
  public function wp_toolbar_message($wp_admin_bar)
  {
    $args = array(
      'title' =>  sprintf(__('Conditional Tags: %s', 'vass-test'), $this->current_tag),
    );
    $wp_admin_bar->add_node($args);

    remove_action('admin_bar_menu', array($this, 'wp_toolbar_message'), 999);
  }

  /**
   * Setup toolbar conditional tag text and hook function
   *
   * @since 1.0.0
   * @return void
   */
  public function setup_toolbar()
  {
    $conditional_tags_found = array();

    foreach ($this->conditional_tags as $key => $value) {
      if (is_array($value)) {
        //check if is an existing method in object
        if (sizeof($value) == 3 && method_exists($value[0], $value[1]) && call_user_func_array(array($value[0], $value[1]), $value[2])) {
          $conditional_tags_found[] = $key;
          //check if is a function
        } else if (sizeof($value) == 2 && function_exists($value[0]) && call_user_func_array($value[0], $value[1])) {
          $conditional_tags_found[] = $key;
        }
      }
    }

    $this->current_tag = empty($conditional_tags_found) ? __('None', 'vass-test') : implode(', ', $conditional_tags_found);

    add_action('admin_bar_menu', array($this, 'wp_toolbar_message'), 999);
  }

  /**
   * Get conditional tags implementation
   *
   * @return ConditionalTagsInterface
   */
  public function get_conditional_tag_imp()
  {
    return $this->conditional_tags_imp;
  }
}
