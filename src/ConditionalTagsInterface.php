<?php

namespace VassTest;

/**
 * Interface definig methods for custom conditional tags
 * 
 * @since 1.0.0
 * @author Adriana <adriana.hregueiro@gmail.com>
 */
interface ConditionalTagsInterface
{

    /**
     * Conditional Tag to check if current page is a post list
     *
     * @since 1.0.0
     * @param string $post_type optional post type to check
     * @return boolean
     */
    public function is_list($post_type = '');

    /**
     * Conditional Tag to check if current page is post edit
     *
     * @since 1.0.0
     * @param string $post_type optional post type to check
     * @return boolean
     */
    public function is_edit($post_type = '');

    /**
     * Conditional Tag to check if current page Settings menu
     *
     * @since 1.0.0
     * @param string $suffix optional page slug suffix (WordPress defaults are: writing, reading, discussion, media, permalink, privacy)
     * @return boolean
     */
    public function is_admin_options_general($suffix = 'general');

    /**
     * Conditional Tag to check if current page is Themes menu
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_themes();

    /**
     * Conditional Tag to check if current page is Dashboard
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_dashboard();
    /**
     * Conditional Tag to check if current page is Plugins
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_plugins();
    /**
     * Conditional Tag to check if current page is Menus
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_nav_menus();

    /**
     * Conditional Tag to check if current page is Users
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_user_list();

    /**
     * Conditional Tag to check if current page is user edit
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_user_edit();

    /**
     * Conditional Tag to check if current page is Tools or
     * one of its submenus
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_tools();
}
