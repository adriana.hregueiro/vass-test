<?php

namespace VassTest;

/**
 * Class containing methods for custom conditional tags
 * 
 * @since 1.0.0
 * @author Adriana <adriana.hregueiro@gmail.com>
 */
class ConditionalTags implements ConditionalTagsInterface
{

    /**
     * Conditional Tag to check if current page is a post list
     *
     * @since 1.0.0
     * @param string $post_type optional post type to check
     * @return boolean
     */
    public function is_list($post_type = '')
    {
        global $pagenow;
        $result = $pagenow == 'edit.php';
        if ($result && !empty($post_type)) {
            $result &= isset($_GET['post_type']) && $_GET['post_type'] == $post_type;
        }
        return $result;
    }

    /**
     * Conditional Tag to check if current page is post edit
     *
     * @since 1.0.0
     * @param string $post_type optional post type to check
     * @return boolean
     */
    public function is_edit($post_type = '')
    {
        global $pagenow;

        $result = $pagenow == 'post.php' && isset($_GET['action']) && $_GET['action'] == 'edit';
        if ($result && !empty($post_type)) {
            $result &= get_post_type() == $post_type;
        }
        return $result;
    }

    /**
     * Conditional Tag to check if current page Settings menu
     *
     * @since 1.0.0
     * @param string $suffix optional page slug suffix (WordPress defaults are: writing, reading, discussion, media, permalink, privacy)
     * @return boolean
     */
    public function is_admin_options_general($suffix = 'general')
    {
        global $pagenow;

        $result = $pagenow == "options-$suffix.php";
        return $result;
    }

    /**
     * Conditional Tag to check if current page Themes menu
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_themes()
    {
        global $pagenow;
        return $pagenow == 'themes.php';
    }

    /**
     * Conditional Tag to check if current page is Dashboard
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_dashboard()
    {
        global $pagenow;
        return is_admin() && $pagenow == 'index.php';
    }

    /**
     * Conditional Tag to check if current page is Plugins
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_plugins()
    {
        global $pagenow;
        return $pagenow == 'plugins.php';
    }

    /**
     * Conditional Tag to check if current page is Menus
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_nav_menus()
    {
        global $pagenow;
        return $pagenow == 'nav-menus.php';
    }

    /**
     * Conditional Tag to check if current page is Users
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_user_list()
    {
        global $pagenow;
        return $pagenow == 'users.php';
    }

    /**
     * Conditional Tag to check if current page is user edit
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_user_edit()
    {
        global $pagenow;
        return $pagenow == 'user-edit.php';
    }

    /**
     * Conditional Tag to check if current page is Tools or
     * one of its submenus
     *
     * @since 1.0.0
     * @return boolean
     */
    public function is_tools()
    {
        global $pagenow;

        return
            $pagenow == "tools.php" ||
            $pagenow == "import.php" ||
            $pagenow == "export.php" ||
            $pagenow == "site-health.php" ||
            $pagenow == "export-personal-data.php" ||
            $pagenow == "erase-personal-data.php";
    }
}
