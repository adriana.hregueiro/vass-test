<?php

/**
 * The plugin bootstrap file
 *
 * @link              https://gitlab.com/adriana.hregueiro/vass-test
 * @since             1.0.0
 * @package           vass-test
 *
 * @wordpress-plugin
 * Plugin Name:       Vass Test
 * Plugin URI:        https://gitlab.com/adriana.hregueiro/vass-test
 * Description:       Custom Conditional Tags
 * Version:           1.0.0
 * Author:            Adriana Hernández Regueiro
 * Author URI:        https://gitlab.com/adriana.hregueiro
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       vass-test
 * Domain Path:       /languages
 */

use VassTest\ConditionalTags;
use VassTest\ConditionalTagsInterface;
use VassTest\VassTestPlugin;

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

require_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';

$vass_test_plugin = new VassTestPlugin();

/**
 * Init main class of plugin just after all plugins and theme are loaded
 * to allow injection of custom ConditionalTags implementation by third party
 *
 * @return void
 */
function vass_test_init()
{
	// filter to inject a diferent implementation of ConditionalTagsInterface if needed
	$imp = apply_filters('vass-test-conditional-tags-instance', new VassTest\ConditionalTags());
	global $vass_test_plugin;
	$vass_test_plugin->init($imp);
}
add_action('wp_loaded', 'vass_test_init');

/**
 * Returns current implementation of conditional tags
 *
 * @return ConditionalTagsInterface
 */
function conditional_tags_imp()
{
	global $vass_test_plugin;
	return $vass_test_plugin->get_conditional_tag_imp();
}